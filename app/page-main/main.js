'use strict';

angular.module('myApp.main', ['ngRoute','ngMessages'])

.controller('MainCtrl', ['$scope', function($scope) {
    $scope.showHints = true;
}]);