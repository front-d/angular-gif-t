angular.module('myApp')
    .config(function($routeProvider) {
        $routeProvider
            .when('/list', {
                templateUrl: 'page-list/list.html',
                controller: 'ListCtrl',
                className: 'overflow'
            })
            .when('/details', {
                templateUrl: 'page-details/details.html',
                controller: 'DetailsCtrl',
                className: ''
            })
            .when('/favorite', {
                templateUrl: 'page-favorite/favorite.html',
                controller: 'ListFavorite',
                className: ''
            })
            .when('/', {
                templateUrl: 'page-main/main.html',
                controller: 'MainCtrl',
                className: ''
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .controller('main', function($scope, $route, $location) {
        $scope.$on('$routeChangeSuccess', function(newVal, oldVal) {
            if (oldVal !== newVal) {
                $scope.routeClassName = $route.current.className;
            }
        })
    });