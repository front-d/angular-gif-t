'use strict';

angular.module('myApp.details', ['ngRoute'])

.controller('DetailsCtrl',  ['$scope', '$cookies', function($scope, $cookies) {
    $scope.favoriteCookie = $cookies.get('linkSelectedGif');
}]);