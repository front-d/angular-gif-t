angular.module('saveBut-directive',[])
    .directive('saveBut', ['$compile','$filter','$cookies','FavoriteFactory', function($compile, $filter, $cookies, FavoriteFactory) {
        return {
            restrict: 'A',
            template: [
                '<md-button class="md-fab md-primary" aria-label="add">',
                '<md-icon md-svg-src="img/icons/add_circle.svg"></md-icon>',
                '</md-button>'
            ].join(''),
            scope: {
                saveBut:'@'
            },
            compile: function() {
                var data = $cookies.getObject('linkSaveGif');
                return function(scope, elem, attrs) {
                        elem.on('click', function(){
                                elem.children().attr('disabled', true);
                                FavoriteFactory(attrs.saveBut);
                        });
                        var gradeC = $filter('filter')(data, attrs.saveBut)[0];
                        if (gradeC) {
                            elem.children().attr('disabled', true);
                        }

                }
            }
        }
    }]);