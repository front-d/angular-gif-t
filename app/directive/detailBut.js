angular.module('detailBut-directive',[])
    .directive('detailBut', ['$compile', '$cookies', '$location', function($compile,$cookies,$location) {
        return {
            restrict: 'A',
            template: [
                '<md-button class="md-fab md-primary" aria-label="view">',
                '<md-icon md-svg-src="img/icons/pageview.svg"></md-icon>',
                '</md-button>'
            ].join(''),
            scope: {
                detailBut:'@'
            },
            compile: function() {
                return function(scope, elem, attrs) {
                    elem.on('click', function(){
                        $cookies.put('linkSelectedGif', attrs.detailBut);
                        scope.$apply( function () {
                            $location.path('/details');
                        });
                    });
                }
            }
        }
    }]);