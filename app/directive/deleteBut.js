angular.module('deleteBut-directive',[])
    .directive('deleteBut', ['$compile', '$window', '$filter', '$cookies',  function($compile, $window, $filter, $cookies) {
        return {
            restrict: 'A',
            template: [
                '<md-button class="md-fab" aria-label="delete">',
                '<md-icon md-svg-src="img/icons/delete.svg"></md-icon>',
                '</md-button>'
            ].join(''),
            scope: {
                deleteBut:'@'
            },
            compile: function() {
                var data = $cookies.getObject('linkSaveGif');
                return function(scope, elem, attrs) {
                    elem.on('click', function(){
                        var gradeA = data.indexOf(attrs.deleteBut);
                        data.splice(gradeA, 1);
                        $cookies.remove('linkSaveGif');
                        $cookies.putObject('linkSaveGif', data);
                        scope.$apply( function () {
                            $window.location.reload();
                        });
                    });
                }
            }
        }
    }]);