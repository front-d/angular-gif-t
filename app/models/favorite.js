angular.module('favoriteFactory',[])
    .factory('FavoriteFactory', [ '$cookies', function($cookies) {
        var setCookies = function(msg) {
            var msgs = $cookies.getObject('linkSaveGif');
            if (msgs) {
                msgs.push(msg);
                $cookies.putObject('linkSaveGif',msgs);
            } else {
                var dateGifList = [];
                dateGifList.push(msg);
                $cookies.putObject('linkSaveGif',dateGifList);
            }
        };
        return setCookies;
    }]);