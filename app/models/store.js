angular.module('store',[])
    .service('Store', ['$http','Gif',function($http,Gif) {
        var items = [];
        function Store() {}
        Store.prototype.getPopular = function (countGif,offset) {
            return $http({
                method: 'GET',
                url: 'http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC&limit='+countGif+'&offset='+offset+""
            }).then(function successCallback(response) {
                items = response.data.data;
                return items.map(function (item) {
                    return new Gif(item.id);
                });
            }, function errorCallback(response) {
                console.log(response);
            });
        };
        return new Store();
    }])
    .factory('Gif', [function () {
        function Gif(id) {
            this.id = id;
        }
        return Gif;
    }]);