'use strict';

angular.module('myApp.favorite', ['ngRoute'])

.controller('ListFavorite', ['$scope', '$cookies', '$location', function($scope,$cookies,$location) {

    $scope.currentPage = 0;
    $scope.pageSize = 2;
    $scope.data = $cookies.getObject('linkSaveGif');
    $scope.numberOfPages=function(){
        return Math.ceil($scope.data.length/$scope.pageSize);
    };
}])

.filter('startFrom', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    }
});

