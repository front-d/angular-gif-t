'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
    'myApp.main',
    'myApp.details',
    'myApp.list',
    'myApp.favorite',
    'ngMaterial',
    'favoriteFactory',
    'saveBut-directive',
    'detailBut-directive',
    'deleteBut-directive'
]).
config(['$locationProvider', '$routeProvider',  function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/'});

}]);
