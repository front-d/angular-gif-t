'use strict';

angular.module('myApp.list', ['ngRoute','ngCookies','store'])

.controller('ListCtrl', ['$scope', '$cookies', 'Store', function($scope, $cookies, Store) {
    $scope.gifHtml = [];
    $scope.count = -1;
    $scope.offset = -24;
    $scope.countGif = 24;

    $scope.load = function() {
        $scope.offset = $scope.offset+24;
        Store.getPopular($scope.countGif,$scope.offset).then(function (_items) {
            $scope.gifHtml.push(_items);
            $scope.count++;
            $scope.gifHtml[$scope.count] = _items;
            console.log($scope.gifHtml);
        });
    };

    $scope.load();
}]);

